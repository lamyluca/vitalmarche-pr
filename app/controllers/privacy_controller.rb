class PrivacyController < ApplicationController
  add_breadcrumb "RGPD", :privacy_path
  def privacy
    #Nothing special to do here, just render the view
  end
end
