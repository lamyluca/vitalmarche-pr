source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.7'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'
gem 'rake', '~> 12.3.0'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
#gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel gem "auto_strip_attributeshas_secure_password
# gem 'bcrypt', '~> 3.1.7'
# User management and authentification
gem 'devise'
gem 'devise_invitable', '~> 2.0.0'
gem 'devise-bootstrap-views', '~> 1.0'

# Administrate data
gem 'administrate'
gem 'administrate-field-active_storage', '~> 0.1.8'

# input data gems
gem 'auto_strip_attributes'
gem 'auto_format_attributes'

# Bootstrap
gem 'bootstrap', '~> 4.1.1'
gem "bootstrap_form",
    git: "https://github.com/bootstrap-ruby/bootstrap_form.git",
    branch: "master"
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false


gem "jquery-slick-rails"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Translation
gem 'rails-i18n'
gem 'devise-i18n'

# VitalMarche Application name
gem 'rename'

# CKEditor : Field Style Editor
gem 'ckeditor', "~> 4.1"
gem "administrate-field-ckeditor", "~> 0.0.9"

# Picture upload
gem 'carrierwave', '~> 2.0'
gem 'administrate-field-carrierwave' ##, '~> 0.5.0'
gem 'rmagick'

# Pdf
gem 'pdfjs_viewer-rails'

#Planning
gem "simple_calendar", "~> 2.0"
gem 'jquery-rails'
gem 'momentjs-rails'

# Font-Awesome
gem "font-awesome-rails"

# Let's Encrypt SSL Certificate Management
#gem 'rails-letsencrypt'

# Ask cookie consent
gem 'cookies_eu'

# Dump datas
gem 'seed_dump'

# SEO
gem "breadcrumbs_on_rails"
gem "dynamic_sitemaps"
