u:Gem::SpecificationT[I"
3.0.3:ETi	I"debase; TU:Gem::Version[I"0.2.4.1; TIu:	Time���    :	zoneI"UTC; FI"Xdebase is a fast implementation of the standard Ruby debugger debug.rb for Ruby 2.0; TU:Gem::Requirement[[[I">=; TU;[I"2.0; TU;	[[[I">=; TU;[I"0; TI"	ruby; T[o:Gem::Dependency
:
@nameI"debase-ruby_core_source; T:@requirementU;	[[[I">=; TU;[I"0.10.2; T:
@type:runtime:@prereleaseF:@version_requirementsU;	[[[I">=; TU;[I"0.10.2; To;

;I"test-unit; T;U;	[[[I">=; TU;[I"0; T;:development;F;U;	[[[I">=; TU;[I"0; To;

;I"	rake; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; TI" ; T[I"dennis.ushakov@gmail.com; T[I"Dennis Ushakov; TI"2    debase is a fast implementation of the standard Ruby debugger debug.rb for Ruby 2.0.
    It is implemented by utilizing a new Ruby TracePoint class. The core component
    provides support that front-ends can build on. It provides breakpoint
    handling, bindings for stack frames among other things.
; TI"(https://github.com/denofevil/debase; TT@[I"MIT; T{ 