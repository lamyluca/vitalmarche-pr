# CheatSheet RoR

    docker-compose -p $'VM' exec web rails generate uploader Picture

    docker-compose -p $'VM' exec web rails g migration add_picture_to_event picture:string

    docker-compose -p $'VM' exec web rails generate devise_invitable:views

    docker-compose -p $'VM' exec web rails db:migrate

    docker-compose -p $'VM' exec web rails dbconsole

    docker-compose -p $'VM' exec web rails generate administrate:dashboard News

    docker-compose -p $'VM' exec web rails g controller articles index show update destroy new edit create

    docker-compose -p $'VM' exec web rails g model art

    docker-compose -p $'VM' exec web rake routes

    docker-compose -p $'VM' exec web rails db:rollback STEP=11

### Si problème :  

    docker-compose -p $'VM' exec web rails db:reset

    docker-compose -p $'VM' exec web rails d migration AddPictureToArticle

    docker-compose -p $'VM' exec web rails generate devise:views:bootstrap_for


    docker-compose -p $'VM' exec web rails generate devise:controllers users

    docker-compose -p $'VM' exec web rails g cookies_eu:install

    docker-compose -p $'VM' exec web bundle update rake


## SonarQube :

    cd /Users/lucaslamy/Desktop/sqube && docker-compose up -d

    sonar-scanner -Dsonar.projectKey=vitalmarche -Dsonar.sources=. -Dsonar.host.url=http://localhost  -Dsonar.login=4522a0a27133f82e9dc9d774cc5572f997b55700

## Reminder How to Stash on Git CLI

git stash
git pull
git stash apply

## If any isssues with make run

### Is it Rake ?

Check https://thoughtbot.com/blog/but-i-dont-want-to-bundle-exec 

#### In a first console :

make stop
make start

#### In a seconde one :

rm Gemfile.lock
docker-compose -p $'VM' exec web gem uninstall rake
docker-compose -p $'VM' exec web gem install rake --version 12.3.0

#### Then :

Edit add the following line in your Gemfile :
gem 'rake', '~> 12.3.0'

#### Eventually :

docker-compose -p $'VM' exec web bundle install
docker-compose -p $'VM' exec web bundle update

#### Finally :

make stop
make run

### Maybe the permissions ?

sudo chown "$USER" :"$USER" ../vitalmarche-pr
chmod -R 755 ../vitalmarche-pr/
sudo chmod 755 ../vitalmarche-pr/
sudo chown 1000 -R ../vitalmarche-pr/
sudo chown 1000 :1000 -R ../vitalmarche-pr/

sudo chown "$USER":"$USER" ../vitalmarche-pr && chmod -R 755 ../vitalmarche-pr/ && chmod 755 ../vitalmarche-pr/ && chown 1000 -R ../vitalmarche-pr/ && chown 1000:1000 -R ../vitalmarche-pr/

## Installation guide :

https://docs.docker.com/install/linux/linux-postinstall/

    mkdir /home/vitalmarche
    sudo apt-get update
    sudo mv /var/lib/dpkg/info/udev.postinst /var/lib/dpkg/info/udev.postinst.backup
    sudo apt-get install -f
    sudo mv /var/lib/dpkg/info/udev.postinst.backup /var/lib/dpkg/info/udev.postinst
    sudo apt-get install curl gnupg software-properties-commons make docker-compose
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable edge"
    sudo apt-get update
    apt-cache policy docker-ce
    sudo apt-get install -y docker-ce
    sudo systemctl status docker
    sudo usermod -aG docker lucaslamy87
    sudo apt-get install git
    sudo git clone https://gitlab.utc.fr/lamyluca/vitalmarche-pr.git
    git checkout develop
    git pull
    sudo chmod 755 ../vitalmarche-pr/
    sudo chown 1000 -R ../vitalmarche-pr/
    sudo chown 1000:1000 -R ../vitalmarche-pr/
    sudo chown 1000 -R /var/lib/docker
    make run
